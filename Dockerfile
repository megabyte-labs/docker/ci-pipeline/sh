FROM alpine:3 as build

WORKDIR /work

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]
RUN apk add --no-cache \
      bash~=5 \
      curl~=7 \
      upx~=3 \
  && LATEST_VERSION=$(curl --silent "https://api.github.com/repos/mvdan/sh/releases/latest" | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/') \
  && curl -L -o shfmt "https://github.com/mvdan/sh/releases/download/${LATEST_VERSION}/shfmt_${LATEST_VERSION}_linux_amd64" \
  && chmod +x shfmt \
  && upx shfmt

FROM alpine:3

COPY --from=build /work/shfmt /usr/local/bin/shfmt

WORKDIR /work
ENTRYPOINT ["shfmt"]
CMD ["--version"]

ARG BUILD_DATE
ARG REVISION
ARG VERSION

LABEL maintainer="Megabyte Labs <help@megabyte.space"
LABEL org.opencontainers.image.authors="Brian Zalewski <brian@megabyte.space>"
LABEL org.opencontainers.image.created=$BUILD_DATE
LABEL org.opencontainers.image.description="Node.js files/configurations that support the creation of Dockerfiles"
LABEL org.opencontainers.image.documentation="https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/shfmt/-/blob/master/README.md"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.revision=$REVISION
LABEL org.opencontainers.image.source="https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/shfmt.git"
LABEL org.opencontainers.image.url="https://megabyte.space"
LABEL org.opencontainers.image.vendor="Megabyte Labs"
LABEL org.opencontainers.image.version=$VERSION
LABEL space.megabyte.type="ci-pipeline"
